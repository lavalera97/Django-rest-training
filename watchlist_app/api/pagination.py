from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination


class WatchListPagination(PageNumberPagination):
    page_size = 5
    page_query_param = 'p'
    page_size_query_param = 'size'


class WatchListLimitOff(LimitOffsetPagination):
    default_limit = 5
    max_limit = 20
    limit_query_param = 'limit'
    offset_query_param = 'start'
