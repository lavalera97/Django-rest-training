from django.contrib import admin
from .models import StreamPlatform, WatchList, Review

# Register your models here.


class AdminWatchList(admin.ModelAdmin):
    list_display = ('title', 'storyline', 'active', 'created')
    list_editable = ('active',)
    search_fields = ('title',)


class WatchListInline(admin.TabularInline):
    model = WatchList
    extra = 0


class AdminStreamPlatform(admin.ModelAdmin):
    inlines = [WatchListInline,]
    list_display = ('name', 'about', 'website')
    list_display_links = ('name', 'website')
    search_fields = ('name', 'website')


class AdminReview(admin.ModelAdmin):
    list_display = ('watchlist', 'review_user', 'rating', 'description', 'active', 'created')
    list_editable = ('active',)
    ordering = ('watchlist',)


admin.site.register(StreamPlatform, AdminStreamPlatform)
admin.site.register(WatchList, AdminWatchList)
admin.site.register(Review, AdminReview)
