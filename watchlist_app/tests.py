from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from watchlist_app.api import serializers
from watchlist_app import models


class StreamPlatformTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='example', password='123123')
        self.token = Token.objects.get(user__username=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

        self.stream = models.StreamPlatform.objects.create(name='Amazon Prime',
                                                           about='Description 1', website='http://amazon.com')

    def test_stream_platform_create(self):
        data = {
            'name': 'Netflix',
            'about': 'Description 1',
            'website': 'http://netflix.com'
        }
        response = self.client.post(reverse('streamplatform-list'), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_stream_platform_put(self):
        data = {
            'name': 'Netflix',
            'about': 'Description 1',
            'website': 'http://netflix.com'
        }
        response = self.client.post(reverse('streamplatform-detail', args=(self.stream.id,)), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_stream_platform_list(self):
        response = self.client.get(reverse('streamplatform-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_stream_platform_ind(self):
        response = self.client.get(reverse('streamplatform-detail', args=(self.stream.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class WatchListTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='example', password='123123')
        self.token = Token.objects.get(user__username=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.stream = models.StreamPlatform.objects.create(name='Amazon Prime',
                                                           about='Description 1', website='http://amazon.com')
        self.watchlist = models.WatchList.objects.create(platform=self.stream, title='Example movie',
                                                         storyline='Description 2', active=True)

    def test_watchlist_create(self):
        data = {
            'platform': self.stream,
            'title': 'Example title',
            'storyline': 'Example story',
            'active': True,
        }
        response = self.client.post(reverse('movie-list'), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_watchlist_list(self):
        response = self.client.get(reverse('movie-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_watchlist_ind(self):
        response = self.client.get(reverse('movie-details', args=(self.watchlist.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.WatchList.objects.count(), 1)
        self.assertEqual(models.WatchList.objects.get().title, 'Example movie')


class ReviewTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='example', password='123123')
        self.token = Token.objects.get(user__username=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.stream = models.StreamPlatform.objects.create(name='Amazon Prime',
                                                           about='Description 1', website='http://amazon.com')
        self.watchlist = models.WatchList.objects.create(platform=self.stream, title='Example movie',
                                                         storyline='Description 1', active=True)
        self.watchlist2 = models.WatchList.objects.create(platform=self.stream, title='Example movie 2',
                                                         storyline='Description 2', active=True)
        self.review = models.Review.objects.create(review_user=self.user, rating=5, description='Some description',
                                                   watchlist=self.watchlist2, active=True)

    def test_review_create(self):
        data = {
            'rating': 5,
            'description': 'Cool movie',
            'watchlist': self.watchlist,
        }
        response = self.client.post(reverse('review-create', args=(self.watchlist.id,)), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


